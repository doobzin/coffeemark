﻿using NUnit.Framework;
using CoffeeMark.Common.Test.Data;

namespace CoffeeMark.Tests
{
    public class CoffeeBuilderTests
    {
        [Test]
        public void CoffeeBuilder_Build_ExpectCoffeBuilderDefault()
        {
            //Arrange
            //Act
            var coffee = new CoffeeBuilder().Build();
            //Assert
            Assert.AreEqual("Americano", coffee.Name);
            Assert.AreEqual(15, coffee.Price);
            Assert.AreEqual(0.1, coffee.Discount);
        }

        [Test]
        public void CoffeeBuilder_Build_ExpectCoffeBuilderDefaultWithDiscount()
        {
            //Arrange
            //Act
            var coffee = new CoffeeBuilder().WithDiscountOf(0.2).Build();
            //Assert
            Assert.AreEqual("Americano", coffee.Name);
            Assert.AreEqual(15, coffee.Price);
            Assert.AreEqual(0.2, coffee.Discount);
        }
    }
}
