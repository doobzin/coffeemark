﻿namespace CoffeeMark.Common.Test.Data
{
    public class CoffeeBuilder
    {
        public string _name { get; private set; }
        public int _price { get; private set; }
        public double _discount { get; private set; }

        public CoffeeBuilder()
        {
            _name = "Americano";
            _price = 15;
            _discount = 0.1;
        }


        public CoffeeBuilder WithDiscountOf(double newDiscount)
        {
            _discount = newDiscount;
            return this;
        }
        
        public Coffee Build()
        {
            return new Coffee(_name, _price, _discount);
        }
    }
}