﻿using System;

namespace CoffeeMark.Common.Test.Data
{
    public class Coffee
    {
        private string name;
        private int price;
        private double discount;

        public Coffee(string name, int price, double discount)
        {
            this.name = name;
            this.price = price;
            this.discount = discount;
        }


        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Price 
        {
            get { return price; }
            set { price = value; } 
        }
        public double Discount 
        { 
            get { return discount; } 
            set { discount = value; } 
        }
    }
}