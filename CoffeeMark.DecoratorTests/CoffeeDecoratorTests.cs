using CoffeeMark.Common.Interfaces;
using CoffeeMark.Decorator;
using NUnit.Framework;

namespace CoffeeMark.DecoratorTests
{
    public class CoffeeDecoratorTests
    {

        [Test]
        [Author("Siphamandla Dube")]
        [Category("Integration")]
        public void CoffeeDecorator_GetCoffee_ExpectCappuccino()
        {
            ICoffeeComponet espresso = new CoffeeComponent(name: "Espresso", quantity: 150, price: 5);
            ICoffeeComponet milkFoam = new CoffeeComponent(name: "Milk Foam", quantity: 50, price: 3);
            ICoffeeComponet hotMilk = new CoffeeComponent(name: "Hot Milk", quantity: 60, price: 3);
            
            //------act
            ICoffeeDecorator sut = new CoffeeDecorator(coffeeName: "Cappuccino");
            sut.AddComponent(espresso);
            sut.AddComponent(milkFoam);
            sut.AddComponent(hotMilk);

            ICoffeeDecorator cappuccino = sut.GetCoffee();

            Assert.AreEqual("Cappuccino", cappuccino.Name);
            Assert.AreEqual(11, cappuccino.Price);
            
            var description = cappuccino.Description;
            Assert.AreEqual(3, description.Count);
            Assert.AreEqual("Espresso", description[0]);
            Assert.AreEqual("Milk Foam", description[1]);
            Assert.AreEqual("Hot Milk", description[2]);

        }
    }

    
    

    


    

}