﻿using CoffeeMark.Common.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace CoffeeMark.Decorator
{
    public class CoffeeDecorator : ICoffeeDecorator
    {
        private string coffeeName;

        public CoffeeDecorator(string coffeeName)
        {
            this.coffeeName = coffeeName;
        }

        private List<ICoffeeComponet> coffeeComponets { get; set; } = new List<ICoffeeComponet>();

        public string Name => coffeeName;

        public List<string> Description => GetCoffeeDescription();

        private List<string> GetCoffeeDescription()
        {
            var desc = new List<string>();
            coffeeComponets.ForEach(o => desc.Add(o.Name));
            return desc;
        }

        public int Price => GetTotalPrice();

        private int GetTotalPrice()
        {
            var price = 0;
            coffeeComponets.ForEach(o => price += o.Price);
            return price;
        }

        public void AddComponent(ICoffeeComponet coffeeMaker)
        {
            coffeeComponets.Add(coffeeMaker);
        }

        public ICoffeeDecorator GetCoffee()
        {
            return this;
        }
    }

}
