﻿using CoffeeMark.Common.Interfaces;
using System;

namespace CoffeeMark.Decorator
{
    [Serializable]
    public class CoffeeComponent : ICoffeeComponet
    {
        private string name;
        private int quantity;
        private int price;

        public CoffeeComponent(string name, int quantity, int price)
        {
            this.name = name;
            this.quantity = quantity;
            this.price = price;
        }

        public string Name => this.name;

        public int Quantity => this.quantity;

        public int Price => this.price;
    }
}
