﻿using System.Collections.Generic;

namespace CoffeeMark.Common.Interfaces
{
    public interface ICoffeeDecorator : ICoffeeMaker
    {
        List<string> Description { get; }

        void AddComponent(ICoffeeComponet coffeeMaker);
        ICoffeeDecorator GetCoffee();
    }
}
