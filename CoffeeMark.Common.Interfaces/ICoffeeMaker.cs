﻿namespace CoffeeMark.Common.Interfaces
{
    public interface ICoffeeMaker
    {
        string Name { get; }
        int Price { get; }
    }
}
