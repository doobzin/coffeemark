﻿namespace CoffeeMark.Common.Interfaces
{

    public interface ICoffeeComponet : ICoffeeMaker
    {
        int Quantity { get; }
    }
}
